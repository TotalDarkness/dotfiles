#!/bin/bash

TMP_COVER="/tmp/cover.png"
COVER="/tmp/album_cover.png"
COVER_SIZE="350"

song="$(mpc --format %file% current)"
[ -z "$song" ] && > $TMP_COVER || mpc -q readpicture "$song" > $TMP_COVER
[ -s $TMP_COVER ] && art=$TMP_COVER || art="$XDG_CONFIG_HOME/ncmpcpp/default_cover.png"
ffmpeg -loglevel 0 -y -i "$art" -vf "scale=$COVER_SIZE:-1" "$COVER"
