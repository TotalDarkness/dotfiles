# TotalDarkness dotfiles

These are my config files for my arch linux installation. Planning on making this to save configurations as I use linux.

Planning to use this to sort and get a better understanding of all my configs so when the time comes to reinstall and start fresh I will at least have my configs and desired stuff.

I will need to remove some files that are specific to the installation I have. I do plan on changing installations like Desktop Enviornments (Using KDE).

## TODO

- [x] Add configs
- [x] Add misselanious
- [x] Look at .locales
- [ ] Write better todo list
- [ ] Make a folder with changed themes, not found $HOME
- [ ] Make a file with changed configs, not found $HOME