#
# ~/.bashrc
#
export XDG_CONFIG_HOME="$HOME/.config"
export EDITOR="vim"
export VISUAL="vim"

PATH=$PATH:$HOME/Dev/Scripts

alias ls='ls --color=auto'
alias list='ls -la'
alias restartKwin='kwin_x11 --replace &'
alias refresh='clear && neofetch'
alias audiodl='youtube-dl --audio-format mp3 --audio-quality 0 -x'
alias mountapple='ifuse -o allow_other $HOME/.local/share/Apple'

PS1='\[\033[38;5;57m\][\[\033[38;5;198m\]\t\[\033[38;5;57m\]] \[\033[38;5;105m\]\u\[\033[38;5;45m\] \w \[\033[38;5;200m\]>\[\033[00m\] ' 
#PS1='\[\033[38;5;57m\][\[\033[38;5;198m\]\t\[\033[38;5;57m\]]\[\033[38;5;45m\] \w \[\033[38;5;200m\]>\[\033[00m\] ' 

if [ -n "$DESKTOP_SESSION" ];then
    eval $(gnome-keyring-daemon --start)
    export SSH_AUTH_SOCK
fi

# If running interactively, do stuff
if [[ $- == *i* ]]; then
neofetch
fi

export QSYS_ROOTDIR="/home/totaldarkness/.cache/paru/clone/quartus-free/pkg/quartus-free-quartus/opt/intelFPGA/20.1/quartus/sopc_builder/bin"
