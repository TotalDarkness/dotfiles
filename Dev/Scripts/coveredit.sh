#!/bin/bash
coverImage=$1
replace=$2

if [ $# -eq 0 ]; then
    echo "No cover image file supplied" && exit
fi

mkdir covers -p

for FILE in *".mp3" ; do
ffmpeg -i "${FILE}" -i "${coverImage}" -map 0:0 -map 1:0 -c copy -id3v2_version 3 -metadata:s:v title="Album cover" -metadata:s:v comment="Cover (front)" "covers/${FILE}"
if [[ $replace == "-y" ]]; then
	mv -fv "covers/${FILE}" "${FILE}"
fi
done

if [[ $replace == "-y" ]]; then
	rm -rv covers
fi
