#!/bin/bash
WID=`xdotool search "Mozilla Firefox" | head -1`
xdotool windowactivate $WID
xdotool click 3
xdotool key a
url=$(xclip -selection c -o)
mpv $url & notify-send -t 5000 -a "$(youtube-dl $url -e)" -u normal --icon=video-television "MPV playing video:" "$url";
#$(curl $(xclip -selection c -o) | grep "<title>")
