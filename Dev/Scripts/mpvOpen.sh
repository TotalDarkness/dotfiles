#!/bin/bash
WID=`xdotool search "Mozilla Firefox" | head -1`
xdotool windowactivate $WID
sleep 0.2
xdotool key ctrl+l+c
xdotool key F6
url=$(xclip -selection c -o)
mpv $url & notify-send -t 5000 -a "$(youtube-dl $url -e)" -u normal --icon=mpv "MPV playing video:" "$url";
