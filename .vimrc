set hidden
set number number
syntax on
autocmd BufWritePost .Xresources :!xrdb %:p
set wildmode=longest,list,full

map <C-d> :silent !$HOME/Dev/Scripts/opener.sh %<CR>:redraw!<CR>
map <C-s> :w<CR>:silent !$HOME/Dev/Scripts/compiler.sh %<CR>:redraw!<CR>
